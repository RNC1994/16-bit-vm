#include "../includes/Assembler.h"

#include <fstream>
#include <iostream>

Assembler::Assembler()
{
	buildSets();
	ALL_FUNCTIONS(BUILD_ASSEMBLY_MAP)
	ALL_FUNCTIONS(LOAD_FLAGS)
}

void Assembler::buildSets()
{
	doneSet.insert("return");
	doneSet.insert("halt");
	doneSet.insert("noop");

	nonRDSet.insert("jump");
	nonRDSet.insert("jumpl");
	nonRDSet.insert("jumpe");
	nonRDSet.insert("jumpg");
	nonRDSet.insert("call");
}

void Assembler::assemble(string file)
{
	int i;
	if ((i = file.find(".s")) != file.length() - 2)
		throw Exceptions::General::InputFormatException(".s");

	if (!fileExists(file))
		throw Exceptions::General::FileMissingException(file.c_str());

	ifstream iFstream(file.c_str());
	if (!iFstream || !iFstream.is_open())
		throw Exceptions::General::StreamOpenException("Input");

	ofstream oFstream(file.replace(i, 2, ".o").c_str());
	if (!oFstream || !oFstream.is_open())
		throw Exceptions::General::StreamOpenException("Output");

	for (lineNumber = 1; getline(iFstream, line); lineNumber++) {
		instruction instr;
		instr.i = parseLine();

		if (instr.i == -1)
			continue;

		oFstream << instr.i << endl;
	}


	iFstream.close();
	oFstream.close();
}

int Assembler::parseLine()
{
	if (line.length() <= 0)
		return -1;

	instruction instr;

	start = 0;
	size = min(line.find_first_of("!"), line.length());

	if (start >= size)
		return -1;

	string op = getNextWord(line, start, size);
	cout << op << endl;

	if (opCodes.count(op) <= 0)
		throw Exceptions::Assembler::ParseException(lineNumber, line, "Missing Operand");
	
	instr.i = opCodes[op].i;
	string sArg;
	int iArg;

	if (dictFlags['D'].count(op) > 0)
		instr.f1.RD = getArgument("RD", 0, 3);
	if (dictFlags['S'].count(op) > 0)
		instr.f1.RS = getArgument("RS", 0, 3);
	if (dictFlags['A'].count(op) > 0)
		instr.fAddr.ADDR = getArgument("ADDR", 0, 255);
	if (dictFlags['C'].count(op) > 0)
		instr.fConst.CONST = getArgument("CONST", -127, 128);

	return instr.i;
}

int Assembler::getArgument(const string& sType, const int& min, const int& max)
{
	if (start < 0 || start >= size)
		throw Exceptions::Assembler::ParseException(lineNumber, line, "Expected " + sType);

	string sArg;
	sArg = getNextWord(line, start, size);

	if (sArg.find_first_not_of("-0123456789") != -1)
		throw Exceptions::Assembler::ParseException(lineNumber, sArg, "Expected Type 'int'");

	int integer;
	convStr2Int(sArg, integer);

	if (integer < min || integer > max)
		throw Exceptions::Assembler::ParseException(lineNumber, convInt2Str(integer),
			"Expected Number Between '" + convInt2Str(min) + " - " + convInt2Str(max) + "'");

	return integer;
}