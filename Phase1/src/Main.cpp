#include "../includes/Assembler.h"
#include "../includes/VirtualMachine.h"

#include <iostream>

using namespace std;

int main()
{
	Assembler assembler;
	VirtualMachine vm;
	try {
		assembler.assemble("test.s");
		vm.loadCode("test.o");
	} catch (Exceptions::Assembler::ParseException & e) {
		cout << e.msg() << endl;
	} catch (Exceptions::General::FileMissingException & e) {
		cout << e.msg() << endl;
	} catch (Exceptions::General::InputFormatException & e) {
		cout << e.msg() << endl;
	} catch (Exceptions::General::StreamOpenException & e) {
		cout << e.msg() << endl;
	} catch (Exceptions::VM::MissingOperandException & e) {
		cout << e.msg() << endl;
	}
}
