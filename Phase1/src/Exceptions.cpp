#include "../includes/Exceptions.h"

#include <sstream>

const string Exceptions::General::FileMissingException::msg() const
{
	stringstream ss;
	ss << "File Missing: Unable to Find '" << fileName << "'";
	return ss.str();
}

const string Exceptions::General::InputFormatException::msg() const
{
	stringstream ss;
	ss << "Input Error: Input File Must Be of File Extension '" << extension << "'";
	return ss.str();
}

const string Exceptions::General::StreamOpenException::msg() const
{
	stringstream ss;
	ss << "Failed To Open " << text << " Stream";
	return ss.str();
}

const string Exceptions::Assembler::ParseException::msg() const
{
	stringstream ss;
	ss << "Parse Error [" << line << "]: " << reason << " \"" << text << "\"";
	return ss.str();
}

const string Exceptions::VM::MissingOperandException::msg() const
{
	stringstream ss;
	ss << "Runtime Error: Uknown Operand For Instruction \"" << text << "\" With MiniID \"" << mID << "\"";
	return ss.str();
}