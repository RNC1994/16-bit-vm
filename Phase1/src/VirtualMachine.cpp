#include "../includes/VirtualMachine.h"

#include <fstream>

VirtualMachine::VirtualMachine()
{
	r = vector<int>(REG_FILE_SIZE);
	mem = vector<int>(MEM_SIZE);
	pc = 0;
	ir.i = 0;
	sr.i = 0;
	sp = 0;
	clk = 0;
	isRunning = false;

	ALL_FUNCTIONS(BUILD_FUNC_MAP)
}

void VirtualMachine::loadCode(string file)
{
	cout << "Loading Code" << endl;

	int i;
	if ((i = file.find(".o")) != file.length() - 2)
		throw Exceptions::General::InputFormatException(".o");

	if (!fileExists(file))
		throw Exceptions::General::FileMissingException(file.c_str());

	ifstream iFstream(file.c_str());
	if (!iFstream || !iFstream.is_open())
		throw Exceptions::General::StreamOpenException("Input");

	string line, sArg;
	int iArg;
	int c = 0;

	for (int lN = 1; getline(iFstream, line); lN++) {
		if (iFstream.fail())
			break;

		int start = 0;

		if (start < 0 || start >= line.length())
			continue;

		sArg = getNextWord(line, start, line.length());

		if (sArg.find_first_not_of("-0123456789") != -1)
			throw Exceptions::Assembler::ParseException(lN, sArg, "Expected Type 'int'");

		convStr2Int(sArg, iArg);
		mem[c++] = iArg;
	}

	iFstream.close();

	runCode();
}

void VirtualMachine::runCode()
{
	isRunning = true;
	pc = 0;

	cout << "Running Code" << endl;

	while (isRunning) {
		ir.i = mem[pc++];
		mID.d.OP = ir.f1.OP;
		mID.d.I = ir.f1.I;

		if (funcMap.count(mID.i) <= 0)
			throw Exceptions::VM::MissingOperandException(convInt2Str(ir.i), convInt2Str(mID.i));

		(this->*funcMap[mID.i])();
	}
}

void VirtualMachine::f_load()
{
	r[ir.fAddr.RD] = mem[ir.fAddr.ADDR];
}

void VirtualMachine::f_loadi()
{
	r[ir.fConst.RD] = ir.fConst.CONST;
}

void VirtualMachine::f_store()
{
	mem[ir.fAddr.ADDR] = r[ir.fAddr.RD];
}

void VirtualMachine::f_add()
{
	r[ir.f1.RD] += r[ir.f1.RS];
}

void VirtualMachine::f_addi()
{
	r[ir.fConst.RD] += ir.fConst.CONST;
}

void VirtualMachine::f_addc()
{
	r[ir.f1.RD] += r[ir.f1.RS] + sr.d.C;
}

void VirtualMachine::f_addci()
{
	r[ir.fConst.RD] += ir.fConst.CONST + sr.d.C;
}

void VirtualMachine::f_sub()
{
	r[ir.f1.RD] -= r[ir.f1.RS];
}

void VirtualMachine::f_subi()
{
	r[ir.fConst.RD] -= ir.fConst.CONST;
}

void VirtualMachine::f_subc()
{
	r[ir.f1.RD] -= ir.f1.RS - sr.d.C;
}

void VirtualMachine::f_subci()
{
	r[ir.fConst.RD] -= ir.fConst.CONST - sr.d.C;
}

void VirtualMachine::f_and()
{
	r[ir.f1.RD] = r[ir.f1.RD] & r[ir.f1.RS];
}

void VirtualMachine::f_andi()
{
	r[ir.fConst.RD] = r[ir.fConst.RD] & ir.fConst.CONST;
}

void VirtualMachine::f_xor()
{
	r[ir.f1.RD] = r[ir.f1.RD] ^ r[ir.f1.RS];
}

void VirtualMachine::f_xori()
{
	r[ir.fConst.RD] = r[ir.fConst.RD] ^ ir.fConst.CONST;
}

void VirtualMachine::f_compl()
{
	r[ir.f1.RD] = ~r[ir.f1.RD];
}

void VirtualMachine::f_shl()
{
	r[ir.f1.RD] = r[ir.f1.RD] << 1;
}

void VirtualMachine::f_shla()
{
}

void VirtualMachine::f_shr()
{
	r[ir.f1.RD] = r[ir.f1.RD] >> 1;
}

void VirtualMachine::f_shra()
{
}

void VirtualMachine::f_compr()
{
	sr.d.L = 0; sr.d.E = 0; sr.d.G = 0;
	if (r[ir.f1.RD] < r[ir.f1.RS])
		sr.d.L = 1;
	else if (r[ir.f1.RD] == r[ir.f1.RS])
		sr.d.E = 1;
	else if (r[ir.f1.RD] < r[ir.f1.RS])
		sr.d.G = 1;
}

void VirtualMachine::f_compri()
{
	sr.d.L = 0; sr.d.E = 0; sr.d.G = 0;
	if (r[ir.fConst.RD] < ir.fConst.CONST)
		sr.d.L = 1;
	else if (r[ir.fConst.RD] == ir.fConst.CONST)
		sr.d.E = 1;
	else if (r[ir.fConst.RD] < ir.fConst.CONST)
		sr.d.G = 1;
}

void VirtualMachine::f_getstat()
{
	r[ir.f1.RD] = sr.i;
}

void VirtualMachine::f_putstat()
{
	sr.i = r[ir.f1.RD];
}

void VirtualMachine::f_jump()
{
	pc = ir.fAddr.ADDR;
}

void VirtualMachine::f_jumpl()
{
	if (sr.d.L)
		pc = ir.fAddr.ADDR;
}

void VirtualMachine::f_jumpe()
{
	if (sr.d.E)
		pc = ir.fAddr.ADDR;
}

void VirtualMachine::f_jumpg()
{
	if (sr.d.G)
		pc = ir.fAddr.ADDR;
}

void VirtualMachine::f_call()
{
	VMStatus stat;
	stat.ADDR = pc;
	stat.r0 = r[0];
	stat.r1 = r[1];
	stat.r2 = r[2];
	stat.r3 = r[3];
	actRecords.push_back(stat);
	pc = ir.fAddr.ADDR;
}

void VirtualMachine::f_return()
{
	VMStatus stat = actRecords.back();
	pc = stat.ADDR;
	r[0] = stat.r0;
	r[1] = stat.r1;
	r[2] = stat.r2;
	r[3] = stat.r3;
	actRecords.pop_back();
}

void VirtualMachine::f_read()
{
}

void VirtualMachine::f_write()
{
}

void VirtualMachine::f_halt()
{
	isRunning = false;
}

void VirtualMachine::f_noop()
{
}
