
#ifndef ASSEMBLER_H
#define ASSEMBLER_H

#include "Defs.h"
#include "Exceptions.h"
#include "Util.h"

#include <map>
#include <set>
#include <string>
#include <sstream>

using namespace std;

class Assembler {
private:
	int lineNumber;
	string line;

	int start;
	int size;

	map<string, instruction> opCodes;
	map<char, set<string> > dictFlags;
	set<string> doneSet;
	set<string> nonRDSet;

	void buildSets();
	int parseLine();
	int getArgument(const string&, const int&, const int&);
public:
	Assembler();
	void assemble(string);
};

#endif
