#ifndef DEF_H
#define DEF_H

/*===============================
=            Structs            =
===============================*/

/*==========  Instruction Struct  ==========*/


struct format1 {
	unsigned UNUSED:6;
	unsigned RS:2;
	unsigned I:1;
	unsigned RD:2;
	unsigned OP:5;
};

struct formatAddr {
	unsigned ADDR:8;
	unsigned I:1;
	unsigned RD:2;
	unsigned OP:5;
};

struct formatConst {
	int CONST:8;
	unsigned I:1;
	unsigned RD:2;
	unsigned OP:5;
};

union instruction {
	int i;
	format1 f1;
	formatAddr fAddr;
	formatConst fConst;
};

/*==========  Mini ID Struct  ==========*/

struct ID {
	unsigned I:1;
	unsigned OP:5;
};

union miniID {
	int i;
	ID d;
};

/*==========  Status Register Struct  ==========*/

struct statusReg {
	unsigned C:1;
	unsigned G:1;
	unsigned E:1;
	unsigned L:1;
	unsigned V:1;
};

union sReg {
	int i;
	statusReg d;
};

/*==========  VM Status Struct  ==========*/


struct VMStatus {
	unsigned ADDR:8;
	int r0:16;
	int r1:16;
	int r2:16;
	int r3:16;
};

/*-----  End of Structs  ------*/

/*===============================
=            Macros            =
===============================*/

/* Formatting Is As Follows
	F(<NAME>, <MiniID>, <INSTRUCTION_ID>, <FLAGS>)

	<MiniID> = Identification Using OP and I bits
	<INSTRUCTION_ID> = Identification Using Instruction Struct
	<FLAGS> = 'D' = RD, 'S' = RS, 'A' = ADDR, 'C' = CONST
*/

#define ALL_FUNCTIONS(F) \
	F(load, 0, 0, DA) \
	F(loadi, 1, 256, DC) \
	F(store, 3, 2304, DA) \
	F(add, 4, 4096, DS) \
	F(addi, 5, 4352, DC) \
	F(addc, 6, 4096, DS) \
	F(addci, 7, 4352, DC) \
	F(sub, 8, 8192, DS) \
	F(subi, 9, 8448, DC) \
	F(subc, 10, 8192, DS) \
	F(subci, 11, 8448, DC) \
	F(and, 12, 12288, DS) \
	F(andi, 13, 12544, DC) \
	F(xor, 14, 14336, DS) \
	F(xori, 15, 14592, DC) \
	F(compl, 16, 16384, D) \
	F(shl, 18, 18432, D) \
	F(shla, 20, 20480, D) \
	F(shr, 22, 22528, D) \
	F(shra, 24, 24576, D) \
	F(compr, 26, 26624, DS) \
	F(compri, 27, 26880, DC) \
	F(getstat,28, 28672, D) \
	F(putstat, 30, 30720, D) \
	F(jump, 33, 33024, A) \
	F(jumpl, 35, 35072, A) \
	F(jumpe, 37, 37120, A) \
	F(jumpg, 39, 39168, A) \
	F(call, 41, 41216, A) \
	F(return, 42, 43008,) \
	F(read, 44, 45056, D) \
	F(write, 46, 47104, D) \
	F(halt, 48, 49152,) \
	F(noop, 50, 51200,) \

#define LOAD_FLAGS(N,V,A,F) \
	for(int i = 0; i < sizeof(#F); i++) \
		if (#F[i] != 0) \
			dictFlags[#F[i]].insert(#N); \

#define DECLARE_FUNCTION(N,V,A,F) \
	void f_##N(); \

#define DEFINE_FUNCTION(N,V,A,F) \
	void f_##N(){} \

#define SETUP_FUNCTIONS(FUNC) \
	FUNC(DECLARE_FUNCTION) \

#define BUILD_FUNC_MAP(N,V,A,F) funcMap[V] = &f_##N;
#define BUILD_ASSEMBLY_MAP(N,V,A,F) opCodes[#N].i = A;

/*-----  End of Macros  ------*/

#endif