#ifndef EXCEPTIONS_H
#define EXCEPTIONS_H

#include <string>
#include <exception>

using namespace std;

class BaseException {
public:
	virtual const string msg() const = 0;
};

class Exceptions {
public:
	class General {
	public:
		class FileMissingException : public BaseException {
		private:
			string fileName;
		public:
			FileMissingException(string f) : fileName(f) {}
			virtual const string msg() const;
		};

		class InputFormatException : public BaseException {
		private:
			string extension;
		public:
			InputFormatException(string e) : extension(e) {}
			virtual const string msg() const;
		};			

		class StreamOpenException : public BaseException {
		private:
			string text;
		public:
			StreamOpenException(string t) : text(t) {}
			virtual const string msg() const;
		};
	};

	class Assembler {
	public:
		class ParseException : public BaseException {
		private:
			int line;
			string text;
			string reason;
		public:
			ParseException(int l, string t, string r) : line(l), text(t), reason(r) {}
			virtual const string msg() const;
		};
	};

	class VM {
	public:
		class MissingOperandException : public BaseException {
		private:
			string text;
			string mID;
		public:
			MissingOperandException(string t, string m) : text(t), mID(m) {}
			virtual const string msg() const;
		};
	};
};

#endif