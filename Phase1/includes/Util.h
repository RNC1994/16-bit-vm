#ifndef UTIL_H
#define UTIL_H

#include <iostream>
#include <sstream>
#include <string>
#include <sys/stat.h>

using namespace std;

static string getNextWord(const string &line, int& start, const int& size)
{
	start = line.find_first_not_of("\t ", start);
	int end = min(line.find_first_of("\t ", start), (size_t)size);
	int tmp = start;
	start = end;
	return line.substr(tmp, end - tmp);
}

static void convStr2Int(const string &word, int& integer)
{
	stringstream(word) >> integer;
}

static string convInt2Str(const int& integer)
{
	stringstream ss;
	ss << integer;
	return ss.str();
}

static bool fileExists(const string &file)
{
	struct stat buf;
	return (stat(file.c_str(), &buf) != -1);
}

static string changeExtension(string file, const string &ext)
{
	int i = file.find_last_of(".");
	return file.replace(i, ext.length(), ext);
}

#endif