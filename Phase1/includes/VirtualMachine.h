#ifndef VIRTUALMACHINE_H
#define VIRTUALMACHINE_H

#include "Defs.h"
#include "Exceptions.h"
#include "Util.h"

#include <map>
#include <string>
#include <vector>

using namespace std;

class VirtualMachine {
private:
	static const int REG_FILE_SIZE = 4;
	static const int MEM_SIZE = 256;
	vector<int> r;
	vector<int> mem;
	vector<VMStatus> actRecords;
	int pc;
	instruction ir;
	sReg sr;
	int sp;
	int clk;
	miniID mID;
	bool isRunning;
public:
	VirtualMachine();
	void loadCode(string file);
	void runCode();
protected:
	typedef void (VirtualMachine::*FP)();
	map<int, FP> funcMap;
	SETUP_FUNCTIONS(ALL_FUNCTIONS);
};

#endif